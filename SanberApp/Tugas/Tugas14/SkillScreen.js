import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';

export default class SkillScreen extends Component {
    renderItem(skillData) {
        return(
            <View key={skillData.id} style={styles.listSkill}>
                <Icon style={styles.skillIcon} name={skillData.logoUrl} size={90}/>

                <View style={styles.skillDesc}>

                    <Text style={styles.skillName}>{skillData.skillName}</Text>
                    <Text style={styles.categoryName}>{skillData.categoryName}</Text>
                    <Text style={styles.percentageProgress}>{skillData.percentageProgress}</Text>
                    
                </View>

                <Icon style={styles.skillIcon} name='chevron-right' size={100} />

            </View>
        )
    }

    render() {
        return(
            <View style={styles.container}>
                <StatusBar style={'auto'} />
                <Image source={require('./images/logo.png')} style={styles.logo} />

                <View style={styles.profile}>
                    <Image source={require('./images/about.jpg')} style={styles.profilePicture} />
                    <View style={styles.profileTxt}>
                        <Text style={styles.profileHai}>Hai,</Text>
                        <Text style={styles.profileName}>Uwais Naufal</Text>
                    </View>
                </View>

                <View style={styles.skillHeader}>
                    <Text style={styles.skillTxt}>SKILL</Text>
                </View>

                <View style={styles.skillRow}>

                    <Text style={styles.skillRowTxt}>Framework / Library</Text>
                   
                    <Text style={styles.skillRowTxt}>Bahasa Pemrograman</Text>

                    <Text style={styles.skillRowTxt}>Teknologi</Text>
                   
                </View>

                <FlatList
                style={{width: '100%'}}
                data={data.items}
                renderItem={(skillData) => this.renderItem(skillData.item)}
                keyExtractor={(item) => String(item.id)}
                ItemSeparatorComponent={() => {return (<View style={{height: 10}} />)}} />

            </View>
            
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#FFFFFF',
        flexDirection: 'column',
        alignItems: 'flex-start',
        padding: 15
      },
      logo: { 
          alignSelf: 'flex-end',
          width: '50%',
          height: '10%',
          resizeMode: 'contain'
      },
      profile:{
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems:'center',
    },

    profilePicture: {
        width: '13%', 
        height: '230%',
        borderRadius: 150,
        resizeMode: 'contain'
    },

    profileTxt: {
        flexDirection: 'column',
        alignItems:'flex-start',
        justifyContent: 'center',
        padding: 10
    },
    profileName: {
        color: '#295DAA',
        fontSize: 15,
        fontWeight: "bold"
    },

    skillHeader: {
        marginTop: 10,
        width: '100%',
        borderBottomColor: '#4CA4F5',
        borderBottomWidth: 0.8,
    },

    skillTxt: {
        fontSize: 30,
        color: '#295DAA',
    },

    skillRow: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginBottom: 10
        
    },

    skillRowTxt: {
        height: 25,
        marginTop: 10, marginHorizontal:5,
        borderRadius: 10,
        backgroundColor: '#7CC4FB',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 5, paddingVertical: 3,
        fontSize: 12,
        color: '#295DAA',
        fontWeight: 'bold',
    },

    listSkill: {
        width: '100%',
        height: 125,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 10,
        borderRadius: 10,
        backgroundColor: '#7CC4FB',
        justifyContent: 'flex-start',
        paddingHorizontal: 5, paddingVertical: 3,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 4,},
        shadowOpacity: 2,
        shadowRadius: 10,
        elevation: 5
    
    },

    skillDesc: {
        flexDirection: 'column',
        width: 160,
    },

    skillIcon: {
        color: '#003366',
    },
    
    skillName: {
        fontSize: 24,
        fontWeight: 'bold',
        color: '#003366',
    },

    categoryName: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#295DAA'
    },
    
    percentageProgress: {
        alignSelf: 'flex-end',
        fontSize: 48,
        fontWeight: 'bold',
        color: '#fff'
    },

})

