import React from 'react';
import { StyleSheet, Image, Text, TextInput, View, TouchableOpacity, Linking } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import { MaterialCommunityIcons } from '@expo/vector-icons';
export default class AboutScreen extends React.Component {
    render() {
        return(
            <View style={styles.container}>
            
                <Text style={styles.txt}>
                        Tentang Saya
                </Text>
                <Image source={require('./images/about.jpg')} style={styles.aboutImg} />
                <Text style={styles.nama}>Uwais Naufal Kusuma</Text>
                <Text style={styles.pekerjaan}>Pelajar</Text>

                <View style={styles.body}>

                    <View style={styles.subBody}>
                        <Text style={styles.judulPorto}>
                            Portofolio
                        </Text>

                        <View style={styles.isiPorto}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://gitlab.com/uwaisnaufal')}>
                                <MaterialCommunityIcons style={styles.icon} name="gitlab" size={60} />
                            </TouchableOpacity>
                            <Text style={styles.gitlabTxt}>@uwaisnaufal</Text>
                        </View>
                        
                    </View>

                    <View style={styles.subBody}>
                        <Text style={styles.judulContact}>
                            Contact Me!
                        </Text>

                        <View style={styles.isiContact}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/uwaisnaufal.k')}>
                                <MaterialCommunityIcons style={styles.icon} name="facebook-box" size={60} />
                            </TouchableOpacity>
                            <Text style={styles.gitlabTxt}>uwaisnaufal.k</Text>
                        </View>
                        
                        <View style={styles.isiContact}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/uwaisn.k/')}>
                                <MaterialCommunityIcons style={styles.icon} name="instagram" size={60} />
                            </TouchableOpacity>
                            <Text style={styles.gitlabTxt}>@uwaisn.k</Text>
                        </View>

                        <View style={styles.isiContact}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/UwaisNaufal')}>
                                <MaterialCommunityIcons style={styles.icon} name="twitter" size={60} />
                            </TouchableOpacity>
                            <Text style={styles.gitlabTxt}>@UwaisNaufal</Text>
                        </View>

                    </View>
                    
                </View>
                <StatusBar style="auto" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 35
    },

    body: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: 'center',
        paddingHorizontal: 35,
        fontFamily: 'Roboto'
    },

    txt : {
        color: '#295DAA',
        fontSize: 30,
        fontWeight: "bold"
    },

    aboutImg: {
        width: '100%', 
        marginTop: 20,
        height: 150,
        borderRadius: 150,
        resizeMode: 'contain'
    },

    nama: {
        marginTop: 10,
        color: '#295DAA',
        fontSize: 24,
        fontWeight: "bold"
    },

    pekerjaan: {
        color: '#3F85EF',
        fontSize: 20,
        fontWeight: "bold"
    },

    subBody: {
        width: '100%',
        backgroundColor: "#EFEFEF",
        borderRadius: 10,
        padding : 10,
        flexDirection: 'column',
        marginBottom: 15
    },

    judulPorto: {
        borderBottomWidth: 0.8,
        borderColor: '#c5c5c5',
        fontSize: 18,
        color: '#003366',
    },

    isiPorto: {
        paddingTop: 5,
        paddingHorizontal: 10,
        alignItems: 'center'
    },

    gitlabTxt: {
        fontSize: 15,
        color: '#003366',
        fontWeight: 'bold',
    },

    judulContact: {
        borderBottomWidth: 0.8,
        borderColor: '#c5c5c5',
        fontSize: 18,
        color: '#003366',
    },

    isiContact: {
        flexDirection: 'row',
        paddingTop: 5,
        paddingHorizontal: 10,
        alignItems: 'center',
    },
})