import React from 'react';
import { StyleSheet, Image, Text, TextInput, View, TouchableOpacity } from 'react-native';
import { StatusBar } from 'expo-status-bar';

import { MaterialCommunityIcons } from '@expo/vector-icons';



export default class LoginScreen extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <Image source={require('./images/logo.png')} style={styles.logoImg}/>
                <View style={styles.body}>
                    
                    <Text style={styles.loginTxt}>Log in!</Text>

                    <View style={styles.input}>
                        <MaterialCommunityIcons style={styles.loginIcon} name='email' size={25}  />
                        <TextInput style={styles.inputTxt} placeholder="Email" />
                    </View>
                    
                    <View style={styles.input}>
                        <MaterialCommunityIcons style={styles.loginIcon} name='lock' size={25}  />
                        <TextInput style={styles.inputTxt} placeholder="Password" secureTextEntry={true} />
                    </View>

                    <TouchableOpacity style={styles.button}>
                        <Text style={styles.buttonTxt}>
                            Masuk
                        </Text>
                    </TouchableOpacity>            

                    <TouchableOpacity style={styles.lupa}>
                        <Text>
                            Lupa password?
                        </Text>
                    </TouchableOpacity>

                    <Text style={{marginTop:10}}>Belum punya akun?</Text>
                    <TouchableOpacity style={styles.buttonDaftar}>
                        <Text style={styles.buttonTxt}>
                            Daftar!
                        </Text>
                    </TouchableOpacity>
                </View>
                <StatusBar style="auto" />
            </View>
          );
    } 
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#FFFFFF',
      alignItems: 'center',
      justifyContent: 'center',
      paddingTop: 35
    },
    
    body: {
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: 'center',
        paddingHorizontal: 35,
        fontFamily: 'Roboto'
    },
    
    loginTxt: {
        color: '#000',
        fontSize: 20,
        fontWeight: "bold"
    },

    logoImg: {
        width: '100%', 
        height: 120,
        resizeMode: 'contain'
    },

    input: {
        width: '100%',
        height: 45,
        flexDirection: 'row',
        marginTop: 20,
        backgroundColor: '#F3F3F3',
        borderColor: '#c5c5c5',
        borderRadius: 20,
        borderWidth: 0.5,
    },

    inputTxt: {
        fontSize: 15
    },

    loginIcon: {
        width: 50,
        height: 45,
        color: '#8A8A8A',
        paddingTop: 9, paddingLeft: 13
    },

    button: {
        width: '50%',
        height: 45,
        marginTop: 20,
        borderRadius: 20,
        backgroundColor: '#295DAA',
        alignItems: 'center',
        justifyContent: 'center'
    },
    
    buttonTxt: {
        color: '#FFF',
        fontSize: 20,
        fontWeight: "bold"
    },

    lupa: {
        width: '100%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: '#c5c5c5',
    },

    buttonDaftar: {
        width: '50%',
        height: 45,
        marginTop: 20,
        borderRadius: 20,
        backgroundColor: '#3F85EF',
        alignItems: 'center',
        justifyContent: 'center'
    },

  });
  